# Understanding the hosting situation: nameservers
# Author: Eduard Constantinescu - s1922629

import logging
import os
from task1 import utils as utils

logger = logging.getLogger("TASK1")


def start():
    logger.info('Starting Task 1b...')
    parse_avro_input()
    logger.info('Finished Task 1b!')


def calculate_top_hosters(hosters):
    """
    Find the top 10 hosters for a TLD.
    :param hosters: List of all hosters and their hosted domains, for a TLD.
    :return: A list of top 10 hosters for a TLD.
    """
    hoster_list = [[h, len(hosters[h]), hosters[h]] for h in hosters]
    hoster_list.sort(key=lambda x: x[1], reverse=True)
    return hoster_list[:10]


def calculate_top_hosters_percentage(top_hosters, signed_dns_list):
    """
    Count the percentage of domains with at least one DNSSEC record, for each top hoster.
    :param top_hosters: A list of top 10 hosters for a TLD.
    :param signed_dns_list: A list of domains with DNSSEC records for a TLD.
    :return: A list of results (percentages) for each top hoster.
    """
    results = []
    for hoster in top_hosters:
        signed_count = 0
        for domain in hoster[-1]:
            if domain in signed_dns_list:
                signed_count += 1
        results.append([hoster[0], round(signed_count / hoster[1] * 100, 1)])

    return results


def parse_avro_input():
    """
    Parse avro input files. Namely, these are nameservers files and signed_dns files.
    ! Make sure to have the input files under the following paths:
    - Files containing domains and their nameservers: ./task1/input/nameservers
    - Files containing domains with DNSSEC records: ./task1/input/signed_dns
    """

    # Task required to analyze .net, and ranks 1 (.bank), 3 (.page) and 5 (.dev)
    hoster_dict = {
        'net': {},
        'bank': {},
        'page': {},
        'dev': {}
    }

    signed_dns_dict = {
        'net': [],
        'bank': [],
        'page': [],
        'dev': []
    }

    nameserver_files = [f for f in os.listdir('./task1/input/nameservers')]
    signed_dns_files = [f for f in os.listdir('./task1/input/signed_dns')]

    for f in nameserver_files:
        utils.parse_nameservers(f, hoster_dict)

    for f in signed_dns_files:
        utils.parse_signed_domains(f, signed_dns_dict)

    for tld in hoster_dict:
        top_hosters = calculate_top_hosters(hoster_dict[tld])
        utils.output_results([[h[0]] for h in top_hosters], '{}.csv'.format(tld))

        top_hosters_percentage = calculate_top_hosters_percentage(top_hosters, signed_dns_dict[tld])
        utils.output_results(top_hosters_percentage, '{}_signed_dns.csv'.format(tld))
