# Comparing deployment of DNSSEC between top-level domains
# Author: Eduard Constantinescu - s1922629

import os
import logging
from task1 import utils as utils

logger = logging.getLogger('TASK1')


def start():
    logger.info('Starting Task 1a...')
    parse_avro_input()
    logger.info('Finished Task 1a!')


def calculate_results(dns_dict, signed_dns_dict):
    """
    Calculate results based on the two dictionary objects.
    :param dns_dict: Dictionary containing counts of all domains per TLD.
    :param signed_dns_dict: Dictionary containing counts of all domains per TLD, with DNSSEC records.
    :return: A list of 'relevant' TLD (which have more than 100 total domains, signed or not), together
            with a percentage of domains with DNSSEC records.
    """
    relevant_tld = []
    for tld in dns_dict:
        if dns_dict[tld] >= 100 and signed_dns_dict.get(tld):
            relevant_tld.append((tld, round(signed_dns_dict[tld] / dns_dict[tld] * 100, 1)))
    relevant_tld.sort(key=lambda x: x[1], reverse=True)

    return relevant_tld


def parse_avro_input():
    """
    Parse avro input files. Namely, these are signed_dns files and domain files.
    ! Make sure to have the input files under the following paths:
    - Files containing domains with DNSSEC records: ./task1/input/signed_dns
    - Files containing all domains: ./task1/input/domains

    Check the READ.ME for more information on how the files can be generated before executing this script.
    """
    signed_dns_dict = {}
    dns_dict = {}

    signed_dns_files = [f for f in os.listdir('./task1/input/signed_dns')]
    domains_files = [f for f in os.listdir('./task1/input/domains')]

    for f in domains_files:
        utils.count_domains(f, dns_dict)

    for f in signed_dns_files:
        utils.count_signed_domains(f, signed_dns_dict)

    results = calculate_results(dns_dict, signed_dns_dict)

    utils.output_results(results, 'task_1a_output.csv')


