# Utility functions
# Author: Eduard Constantinescu - s1922629

import csv
import logging
import os
import subprocess
from publicsuffix2 import PublicSuffixList
import codecs

logger = logging.getLogger("UTILS")


def output_results(results, filename):
    """
    Output results to a file.
    :param results: A list of results.
    :param filename: The name of output file.
    """
    with open('./task1/output/' + filename, 'w') as output_file:
        csv_writer = csv.writer(output_file)
        for entry in results:
            csv_writer.writerow(entry)


def count_domains(file, dns_dict):
    """
    Count total domains found in each TLD (.com, .net, .org etc.)
    :param file: Input file where domains can be found.
    :param dns_dict: Dictionary object which stores results.
    """
    with open('./task1/input/domains/' + file, 'r') as domains_file:
        lines = domains_file.readlines()
        for l in lines:
            try:
                splitted = l.split('.')
                tld = splitted[-2]
                if not dns_dict.get(tld):
                    dns_dict[tld] = 1
                else:
                    dns_dict[tld] += 1
            except Exception as e:
                logger.error('Error parsing a domain: {}'.format(l))


def count_signed_domains(file, signed_dns_dict):
    """
    Count number of domains with at least one DNS record signed.
    :param file: File where domains with signed DNS records can be found.
    :param signed_dns_dict: Dictionary object which stores results.
    """
    with open('./task1/input/signed_dns/' + file, 'r') as signed_domains_file:
        lines = signed_domains_file.readlines()
        for l in lines:
            try:
                splitted = l.split('.')
                tld = splitted[-2]
                if not signed_dns_dict.get(tld):
                    signed_dns_dict[tld] = 1
                else:
                    signed_dns_dict[tld] += 1
            except Exception as e:
                logger.error('Error parsing a signed domain: {}'.format(l))


def parse_nameservers(file, hoster_dict):
    """
    Parse the nameserver files. Construct a Dictionary where each required TLD contains hosters and their respective hosted domains.
    :param file: File where domains and nameservers can be found.
    :param hoster_dict: A Dictionary object containing results.
    """
    psl = PublicSuffixList(codecs.open('./public_suffix_list.dat', encoding='utf8'))

    with open('./task1/input/nameservers/'+ file, 'r') as nameserver_file:
        lines = nameserver_file.readlines()
        for i in range(0, len(lines), 2):
            try:
                domain = lines[i][:-2]
                nameserver = lines[i+1][:-2]
                tld = psl.get_tld(domain)

                # Check that the TLD is either .net, or ranks 1, 3 or 5
                if hoster_dict.get(tld) is not None:
                    nameserver_tld = psl.get_tld(nameserver)
                    hoster = nameserver[:-len(nameserver_tld) - 1].split('.')[-1]
                    if not hoster_dict[tld].get(hoster):
                        hoster_dict[tld][hoster] = [domain]
                    elif domain not in hoster_dict[tld][hoster]:
                        hoster_dict[tld][hoster].append(domain)
            except Exception as e:
                logger.error('Error parsing namserver for domain {}!'.format(lines[i]))


def parse_signed_domains(file, signed_dns_dict):
    """
    Parse a file containing domains with DNSSEC records.
    Construct a Dictionary containing a list of domains with DNSSEC records, per required TLDs.
    :param file: File containing domains with DNSSEC records.
    :param signed_dns_dict: Dictionary object containing results.
    """
    with open('./task1/input/signed_dns/' + file, 'r') as signed_domains_file:
        lines = signed_domains_file.readlines()
        for l in lines:
            try:
                splitted = l.split('.')
                tld = splitted[-2]
                if signed_dns_dict.get(tld) is not None:
                    signed_dns_dict[tld].append(l[:-2])
            except Exception as e:
                logger.error('Error parsing a signed domain: {}'.format(l))