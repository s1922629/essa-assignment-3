import csv
import json
import ijson
import gzip


def write_to_json(path, data):
    """
    Write data to a json file.
    :param path: Path of the output file.
    :param data: JSON formatted data.
    """
    with open(path, 'w') as json_file:
        json.dump(data, json_file, indent=1)


def write_to_file(path, data):
    """
    Write data to a file.
    :param path: Path of the output file.
    :param data: List formatted data.
    """
    with open(path, 'w') as output_file:
        for line in data:
            output_file.write(line + '\n')


def parse_tlsa_data():
    """
    Parse TLSA data and domain data from given input files.
    ! Make sure you have the required files on the following paths:
    - Files containing TLSA data: ./task2/input/task2.tlsa_domain.csv
    - Files containing domains data: ./task2/input/task2.domains.csv
    :return:
    - tlsa_data: Dictionary object containing parsed TLSA data
    - found_domains: List of domains that have associated TLSA records
    - all_domains: List of all domains
    """
    tlsa_data = []
    found_domains = []
    all_domains = []

    with open('./task2/input/task2.tlsa_domain.csv', 'r') as tlsa_csv_file:
        tlsa_csv_reader = csv.reader(tlsa_csv_file)
        with open('./task2/input/task2.domains.csv', 'r') as domains_csv_file:
            domains_csv_reader = csv.reader(domains_csv_file)
            all_domains = [row[0] for row in domains_csv_reader]

            for row in tlsa_csv_reader:
                dane_data = row[0].split(' ')
                domain = row[1].split('._tcp.')[-1][:-1]
                if domain in all_domains:
                    tlsa_data.append({
                        'cert_usage': dane_data[0],
                        'selector': dane_data[1],
                        'matching_type': dane_data[2],
                        'cert_association_data': dane_data[3],
                        'domain': row[1].split('._tcp.')[-1][:-1]
                    })

                    if domain not in found_domains:
                        found_domains.append(domain)

    # # Write this data to a .json file for debugging purposes.
    # write_to_json('./task2/input/tlsa_domains_data.json', tlsa_data)

    return tlsa_data, found_domains, all_domains


def parse_zcat_data():
    """
    Parse relevant zcat data.
    ! Make sure to have the following files:
    - zcat data: ./task2/input/zcat_results.json

    Check the READ.ME for information on how to generate this file.
    :return: A list of Dictionary formatted zcat entries.
    """
    zcat_data = []

    with open('./task2/input/zcat_results.json', 'r') as zcat_file:
        zcat_content = zcat_file.read()
        decoder = json.JSONDecoder()

        while zcat_content:
            value, new_start = decoder.raw_decode(zcat_content)
            zcat_content = zcat_content[new_start:].strip()

            zcat_data.append(value)

    return zcat_data
