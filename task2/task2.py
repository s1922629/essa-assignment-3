# Analyzing DANE-TLSA records
# Author: Eduard Constantinescu - s1922629

import logging
import task2.utils as utils
from Crypto.Util.asn1 import DerSequence
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256, SHA512
from binascii import a2b_base64

logger = logging.getLogger('TASK2')


def count_tlsa_domains(found_domains, all_domains):
    """
    Count domains with at least one TLSA record.
    :param found_domains: List of domains with at least one TLSA record.
    :param all_domains: List of all domains.
    """
    logger.info('Total number of domains with TLSA: {}'.format(len(found_domains)))
    logger.info('Total number of domains: {}'.format(len(all_domains)))
    logger.info('Percentage: {} %'.format(len(found_domains) / len(all_domains) * 100))


def count_consistent_certificates(found_domains, zcat_data, tlsa_data):
    """
    Count the number of domains with at least one TLSA record consistent with their certificate(s).
    :param found_domains: List of domains with at least one TLSA record.
    :param zcat_data: List of Dictionary formatted zcat data.
    :param tlsa_data: List of Dictionary formatted TLSA data.
    """
    found = 0
    consistent_domains = []

    for domain in found_domains:
        # Iterate over each domain with at least one TLSA record.

        zcat_entry = [i for i in zcat_data if i['domain'] == domain]
        tlsa_entry = [i for i in tlsa_data if i['domain'] == domain]

        for t in tlsa_entry:
            # Iterate over each TLSA record for this domain.

            check_certificates = True
            for c in zcat_entry:
                # Iterate over each zcat entry for this domain.

                # First get the required certificate from the zcat data.
                content_check = get_certificate(t['cert_usage'], zcat_entry[0])

                if content_check is not None:
                    # Continue only if there exists a certificate.

                    if t['selector'] == '1':
                        # Retrieve the PublicKeyInfo of the certificate.
                        content_check = get_public_key_info(content_check)
                    else:
                        # Convert to binary.
                        content_check = content_check.encode()

                    if compare_association(content_check, t['cert_association_data'], t['matching_type']):
                        # We found a domain with a consistent TLSA record.
                        found += 1
                        consistent_domains.append(domain)
                        check_certificates = False
                        break

            if not check_certificates:
                # If we already found a consistent TLSA record, we can skip to the next domain.
                break

    logger.info('Total number of domains with at least one TLSA record consistent with their certificate(s): {}'.format(found))


def get_certificate(cert_usage, zcat_entry):
    """
    Get the required certificate used for checking against the TLSA record.
    :param cert_usage: TLSA flag.
    - if '0': retrieve the CA certificate
    - if '1' or '3': retrieve the end-certificate
    - if '2': retrieve the anchor certificate
    :param zcat_entry: Dictionary formatted zcat entry containing certificates.
    :return: A raw-formatted certificate. Will return 'None' if no certificate is found.
    """
    try:
        if cert_usage == '0':
            # CA certificate
            return zcat_entry['data']['tls']['result']['handshake_log']['server_certificates']['chain'][0]['raw']
        elif cert_usage == '1' or cert_usage == '3':
            # End entity certificate
            return zcat_entry['data']['tls']['result']['handshake_log']['server_certificates']['certificate']['raw']
        elif cert_usage == '2':
            # Trust anchor certificate
            return zcat_entry['data']['tls']['result']['handshake_log']['server_certificates']['chain'][-1]['raw']
    except Exception as e:
        # Certificate could not be retrieved
        return None


def get_public_key_info(certificate):
    """
    Retrieve the Subject Public Key Info for a raw-certificate.
    :param certificate: Raw certificate.
    :return: The Subject Public Key Info.
    """
    der = a2b_base64(certificate)
    cert = DerSequence()
    cert.decode(der)
    tbsCertificate = DerSequence()
    tbsCertificate.decode(cert[0])
    subjectPublicKeyInfo = tbsCertificate[6]

    return subjectPublicKeyInfo


def compare_association(content, cert_association_data, matching_type):
    """
    Compare the content extracted from the zcat data against the TLSA data.
    :param content: Binary content extracted from the zcat data (full-certificate / Subject Public Key Info)
    :param cert_association_data: TLSA data used for comparison.
    :param matching_type: Determines how the comparison should be made:
    - if '0': compare exact values
    - if '1': compare SHA-256 values
    - if '2': compare SHA-512 values
    :return: True, if the content is equal to the TLSA data. False, otherwise.
    """
    if matching_type == '0':
        return content == cert_association_data.encode()
    elif matching_type == '1':
        h = SHA256.new()
        h.update(content)
        return h.hexdigest() == cert_association_data
    elif matching_type == '2':
        h = SHA512.new()
        h.update(content)
        return h.hexdigest() == cert_association_data


def start():
    logger.info('Starting Task 2...')

    tlsa_data, found_domains, all_domains = utils.parse_tlsa_data()

    count_tlsa_domains(found_domains, all_domains)

    zcat_data = utils.parse_zcat_data()
    count_consistent_certificates(found_domains, zcat_data, tlsa_data)
