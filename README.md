# Empirical Security Analysis & Engineering
## Assignment Group 1 Part 3
### Author: Eduard Constantinescu
### Student Number: s1922629

## Installation

```bash
$ pipenv install
```

## Running the script

```bash
$ pipenv shell
$ python main.py
```

> Check the next section before running the script!

## Important scripts

The script makes use of several input files for both tasks.

For Task 1, these are found on the following paths:
- `./task1/input/domains`
- `./task1/input/nameservers`
- `./task1/input/signed_dns`

For Task 2, these are found on the following path:
- `./task2/input`

To generate all the required input files, run the `input.sh` script found in the root folder:
```bash
$ ./input.sh
```

## Results

### Task 1 a)

Results can be found at the file found at `./task1/output/task_1a_output.csv`

### Task 1 b)

The analyzed TLDs were `.net`, rank 1 (`.bank`), rank 3 (`.dev`), rank 5 (`.page`).

CSV files of top hosters per TLD can be found on the following paths:
- `./task1/output/net.csv`
- `./task1/output/bank.csv`
- `./task1/output/dev.csv`
- `./task1/output/page.csv`

Detailed results:
- `.net` top hosters:
  - `domaincontrol`
  - `cloudflare`
  - `googledomains`
  - `registrar-servers`
  - `ui-dns`
  - `hichina`
  - `worldnic`
  - `dns`
  - `wixdns`
  - `gname`
- `.bank` top hosters:
  - `profitstars`
  - `smbonline`
  - `iowatrustsavings`
  - `lifeafterfishing`
  - `pfbc`
  - `berkley`
  - `bbky`
  - `web5`
  - `heritagebankofcommerce`
  - `firstfdn`
- `.dev` top hosters:
  - `googledomains`
  - `cloudflare`
  - `registrar-servers`
  - `domaincontrol`
  - `nsone`
  - `porkbun`
  - `digitalocean`
  - `gandi`
  - `vercel-dns`
  - `hover`
- `.page` top hosters:
  - `googledomains`
  - `cloudflare`
  - `registrar-servers`
  - `domaincontrol`
  - `alidns`
  - `porkbun`
  - `name`
  - `wordpress`
  - `dan`
  - `gandi`

---

CSV files of top hosters per TLD (with percentage of domains hosted with at least one DNS record signed) can be found on the following path:
- `./task1/output/net_signed_dns.csv`
- `./task1/output/bank_signed_dns.csv`
- `./task1/output/dev_signed_dns.csv`
- `./task1/output/page_signed_dns.csv`

Detailed results:
- `.net` top hosters (with percentage of domains hosted with at least one DNS record signed):
  - `domaincontrol, 0.1`
  - `cloudflare, 6.6`
  - `googledomains, 65.2`
  - `registrar-servers, 1.8`
  - `ui-dns, 0.0`
  - `hichina, 0.0`
  - `worldnic, 0.3`
  - `dns, 0.1`
  - `wixdns, 0.1`
  - `gname, 0.0`
- `.bank` top hosters (with percentage of domains hosted with at least one DNS record signed):
  - `profitstars, 100.0`
  - `smbonline, 100.0`
  - `iowatrustsavings, 100.0`
  - `lifeafterfishing, 100.0`
  - `pfbc, 100.0`
  - `berkley, 100.0`
  - `bbky, 100.0`
  - `web5, 100.0`
  - `heritagebankofcommerce, 100.0`
  - `firstfdn, 100.0`
- `.dev` top hosters (with percentage of domains hosted with at least one DNS record signed):
  - `googledomains, 75.5`
  - `cloudflare, 21.1`
  - `registrar-servers, 3.2`
  - `domaincontrol, 0.0`
  - `nsone, 0.0`
  - `porkbun, 1.6`
  - `digitalocean, 0.0`
  - `gandi, 9.3`
  - `vercel-dns, 0.0`
  - `hover, 0.0`
- `.page` top hosters (with percentage of domains hosted with at least one DNS record signed):
  - `googledomains, 64.4`
  - `cloudflare, 19.5`
  - `registrar-servers, 2.5`
  - `domaincontrol, 0.0`
  - `alidns, 0.0`
  - `porkbun, 0.0`
  - `name, 0.0`
  - `wordpress, 0.0`
  - `dan, 0.0`
  - `gandi, 0.0`

### Task 1 c)

There are a couple of conclusions to be drawn from the output data:
- domains hosted by `googledomains` have a high rate (>60%) of DNSSEC usage, as observed in the `.net, .dev, .page` TLDs
- domains hosted by `cloudflare` have a low rate (<22%) of DNSSEC usage, as observed in the `.net, .dev, .page` TLDs
- `.bank` domains are an interesting outlier where all the domains have a 100% DNSSEC usage; however, the `.bank` hosters are very unusual, thus are not really comparable to the other hosters

Overall, it seems that the only correlation between hoster and DNSSEC usage can be found on the two popular hosters `googledomains` and `cloudflare`.

### Task 2

TLSA records contain 3 fields used to specify how the verification of the record vs certificate must be made:

1) `Certificate Usage`
This field specifies the provided association that will be used to match the certificate. It can be 4 different values:
- `0`: verification must be performed against the CA certificate
- `1`: verification must be performed against the end entity certificate, together with a PKIX certification path validation
- `2`: verification must be performed against the trust anchor certificate
- `3`: verification must be performed against the end entity certificate, without a PKIX validation
2) `Selector`
This field specifies which part of the certificate must be verified. It can be 2 different values:
- `0`: check the full certificate
- `1`: check `SubjectPublicKeyInfo`
3) `Matching Type`
This field specifies how the certificate association is formatted. It can be 3 different values:
- `0`: the association is the exact match of the selected content to be checked against
- `1`: the association is a SHA-256 hash of the selected content to be checked against
- `2`: the association is a SHA-512 hash of the selected content to be checked against

The number of domains that set at least one TLSA record is `218`, or `0.013%` of all domains.

The number of domains that set at least one TLSA record consistent with their certificate(s) is `56`.
