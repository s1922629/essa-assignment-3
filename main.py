# ESAE Assignment Group 1 Part 3
# Author: Eduard Constantinescu - s1922629

import logging
import task1.a, task1.b
import task2.task2 as task2


def main():
    setup_loggers()

    task1.a.start()
    task1.b.start()

    task2.start()


def setup_loggers(verbose=False):
    if verbose:
        logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info("Verbose output enabled!")
    else:
        logging.basicConfig(format="%(asctime)s %(levelname)s: %(message)s", level=logging.INFO)


if __name__ == "__main__":
    main()
